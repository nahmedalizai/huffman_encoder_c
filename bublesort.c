/* Bubble sort code */
 
#include <stdio.h>
 
int main()
{
  int array[5] = {5,3,2,1,4}, swap;
 
  for (int i = 0 ; i < 5; i++)
  {
    for (int j = i ; j < 5; j++)
    {
      if (array[i] > array[j]) /* For decreasing order use < */
      {
        swap       = array[i];
        array[i]   = array[j];
        array[j] = swap;
      }
    }
  }
 
  printf("Sorted list in ascending order:\n");
 
  for (int c = 0; c < 5; c++)
     printf("%d\n", array[c]);
 
  return 0;
}