#include <stdio.h>
int neg_ch_arr[256] = {0};

int add_neg_char(char c) {
    int index_found = 0, new_index=0;
    signed char s_ch;
    for(int i =0; i < 256; i++){
        if(neg_ch_arr[i] == c) {
            index_found++;
            new_index = i;
            break;
        }
    }
    if(index_found == 0) {
        for(int j = 0; j < 256; j++){
            if(neg_ch_arr[j] == 0){
                neg_ch_arr[j] = c;
                new_index = j;
                break;
            }
        }
    }
    return new_index;
}

int get_neg_char_index(char val){
    for(int i =0; i < 256; i++) {
        if(neg_ch_arr[i] == val)
        return i;
    }
    return -1;
}
int main() {  
    int freq[512]={0}, char_count=0, index = 0;
    char ch;
    FILE* fc_input = fopen("pic","rb");

    while((ch = getc(fc_input)) != EOF) {
        if(ch < 0){
            index = add_neg_char(ch);
            freq[index+256] ++;
        }
        else
            freq[ch] ++;
    }
    rewind(fc_input);
    index=0;
    while((ch = getc(fc_input)) != EOF) {
        if(ch < 0){
            index = get_neg_char_index(ch);
            printf("Char : %d , Freq : %d\n",ch,freq[index+256]);
        }
    }
    return 0;
}