#include <stdio.h>

int main ()
{
    char c = 'b';
    for (int i = 7; i >= 0; --i)
    {
        putchar( (c & (1 << i)) ? '1' : '0' );
    }
    putchar('\n');
  return 0;
}