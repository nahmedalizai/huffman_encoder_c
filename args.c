#include <stdio.h>
#include <string.h>

void print_help(void) {
    puts("Usage: huffman (-c|-d) input output");
    puts("  -c    Compress file from input to output");
    puts("  -d    Uncompress file from input to output");
    puts("\nCreated by iBug");
}

int main(int argc, char** argv) {
    if (argc != 4) {
        print_help();
        return 1;
    }
    if (!strcmp(argv[1], "-c")) {
        printf("\nCompress file\n");
    } else if (!strcmp(argv[1], "-d")) {
        printf("\nDecompress file\n");
    } else {
        print_help();
        return 1;
    }

    printf("\nInput file : %s\n", argv[2]);
    printf("\nOutput file : %s\n", argv[3]);
    
    return 0;
}