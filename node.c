#include <stdio.h> 
#include <stdlib.h> 

typedef struct node
{
    int data;
    struct node* left;
    struct node* right;
} node;

node* create_node(int data,int data1)
{
    node *new_node = (node*)malloc(sizeof(node));
    if(new_node == NULL)
    {
        fprintf (stderr, "Out of memory!!! (create_node)\n");
        exit(1);
    }
    new_node->data = data + data1;
    new_node->left = NULL;
    new_node->right = NULL;
    return new_node;
}

typedef int (*comparer)(int, int);


int compare(int left,int right)
{
    if(left > right)
        return 1;
    if(left < right)
        return -1;
    return 0;
}

node* insert_node(node *root, comparer compare, int data, int data1)
{
 
    if(root == NULL)
    {
        root = create_node(data,data1);
    }
    else
    {
        int is_left  = 0;
        int r        = 0;
        node* cursor = root;
        node* prev   = NULL;
 
        while(cursor != NULL)
        {
            r = compare(data,cursor->data);
            prev = cursor;
            if(r < 0)
            {
                is_left = 1;
                cursor = cursor->left;
            }
            else if(r > 0)
            {
                is_left = 0;
                cursor = cursor->right;
            }
 
        }
        if(is_left)
            prev->left = create_node(data,data1);
        else
            prev->right = create_node(data,data1);
 
    }
    return root;
}

#define SIZE 3
 
int main()
{
    node* root = NULL;
    comparer int_comp = compare;
 
    /* insert data into the tree */
    int a[SIZE] = {1,2,3};
    int remaining = SIZE;
    int i=0;
    printf("--- C Binary Search Tree ---- \n\n");
    printf("Insert: ");
    while(remaining > 0)
    {
        printf("%d ",a[i]);
        root = insert_node(root,int_comp,a[i],a[i+1]);
        i++;
        remaining--;
    }

    /* remove the whole tree */
    //dispose(root);
    return 0;
}