// C program for different tree traversals 
#include <stdio.h> 
#include <stdlib.h> 
#define CHAR 256
#define BIT_MAX_SIZE 16
int freq_code[CHAR][BIT_MAX_SIZE]; 
FILE *fwb_ptr;
FILE *fr_ptr;

struct tree {
	int ch;
	int freq;
	int is_leaf; 
	struct tree* left; 
	struct tree* right; 
}; 
struct tree* forest[CHAR] = {NULL};

struct tree* new_tree_node(int ch, int freq, int is_leaf, struct tree* t1, struct tree* t2) {
	struct tree* tree = (struct tree*)malloc(sizeof(struct tree)); 
	tree->ch = ch;
	tree->freq = freq;
	tree->is_leaf = is_leaf; 
	tree->left = t1; 
	tree->right = t2; 

	return(tree); 
} 

void sort_forest(int treeCount) {
	struct tree* temp;
	for(int i =0; i<treeCount; i++){
		for(int j = i; j<treeCount; j++){
			if(forest[i]->freq > forest[j]->freq){
				temp = forest[i];
				forest[i] = forest[j];
				forest[j] = temp;
			}
		}
	}
}

void shift_trees(int index, int length){
	for(int i = index; i < length; i++){
		forest[i] = forest[i+1];
	}
}

struct tree* make_tree(int* freq){
	int treeCount=0;
    for(int i =0; i <CHAR; i++) {
		if(freq[i] > 0){
			forest[treeCount] = new_tree_node(i,freq[i],1, NULL, NULL);
			treeCount++;
		}
	}
	struct tree* t1;
	struct tree* t2;
	if(treeCount == 1){
		t1 = forest[0];
		forest[0] = new_tree_node(0,t1->freq,0,t1,NULL);
	}
	else {
		sort_forest(treeCount);
		// for(int i =0; i <treeCount; i++) {
		// 	printf("Tree %d - CH : %c/%d - Freq : %d \n",i,forest[i]->ch,forest[i]->ch, forest[i]->freq);
		// }
		int is_first_time = 1, i = 0;
		while(treeCount > 1){
			t1 = forest[0];
			t2 = forest[1];
			forest[0] = new_tree_node(0,t2->freq+t1->freq,0,t1,t2);
			shift_trees(1,treeCount);
			treeCount--;
			sort_forest(treeCount);
			is_first_time = 0;
		}
	}
	return forest[0];
}

void putbits(char inbits){
	//fputc(inbits,fpt);
// int bits = 0xFFFF;
// if(inbits == 0){
// 	bits &= 0xFF00;

// }
}

void print_codes(struct tree* root, int arr[], int top) {
    if (root->left) { 
        arr[top] = 0; 
        print_codes(root->left, arr, top + 1); 
    } 
    if (root->right) {
        arr[top] = 1;
        print_codes(root->right, arr, top + 1); 
    } 
    if (root->is_leaf == 1) {
		 
		printf("%c: ", root->ch);
        int i; 
        for (i = 0; i < top; ++i){ 
			freq_code[root->ch][i] = arr[i]+1;
			printf("%d",arr[i]);
		}
    	printf("\n"); 
     }
} 

int main() 
{
	struct tree *binary_tree = (struct tree*)malloc(sizeof(struct tree));
    int freq[CHAR]={0}, char_count=0;
    char c;
    fr_ptr = fopen("input.txt","r");
	fwb_ptr = fopen( "test.bin" , "wb");
    if (fr_ptr == NULL) {
        printf("file not found");
        return 1;
    }
    else {
       while((c = getc(fr_ptr)) != EOF) { 
           freq[c] ++;
		   char_count++;
        }
		fclose(fr_ptr);
		if(char_count > 0){
					binary_tree = make_tree(freq);
			int arr[CHAR], top = 0;
			print_codes(binary_tree,arr,top);
			fr_ptr = fopen("input.txt","r");
			while((c = getc(fr_ptr)) != EOF) { 
           		for(int i=0; i<BIT_MAX_SIZE; i++){
			   		if(freq_code[c][i] != 0){
						if(freq_code[c][i] ==1)
							fputc(0,fwb_ptr);
						else
							fputc(1,fwb_ptr);
			   		}
			   		else
				   		break;
		   		}
        	}
		}
    }
	return 0;
}