#include <stdio.h>
    
void showbits( unsigned int x )
{
    for (int i = 0; i <10; i++)
    {
       putchar(x & (1u << i) ? '1' : '0');
    }
    printf("\n");
}

int main( void )
{
    int j = 50;
    printf("%d in binary \t\t ", j);
    
    /* assume we have a function that prints a binary string when given 
       a decimal integer 
    */
    showbits(j);

    /* the loop for right shift operation */
    for (int m = 0; m <= 5; m++)
    {
        int n = j >> m;
        printf("%d right shift %d gives ", j, m);
        showbits(n);
    }
    return 0;
}