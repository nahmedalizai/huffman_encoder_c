#include <stdio.h>
#include <stdlib.h>
#define CHAR 256

int main()
{
      FILE* fileinput = fopen("file","rb");
      char ch_arr[CHAR] = {0};
      int ch_freq[CHAR] = {0};
      char c;
      int n = 0, exists = 0;
      while((c = getc(fileinput)) != EOF) {
        for(int i=0; i < n; i++){
            if(ch_arr[i] == c) {
                exists = 1;
                ch_freq[i]++;
                break;
            }
        }
        if(exists == 0){
            ch_arr[n] = c;
            ch_freq[n] ++;
            n++;
        }
        exists = 0;
      }
      n = 0;
      for (int i = 0; i < CHAR; i++) {
          if(ch_arr[i] > 0)
            n++;
          else
            break;
      }
}