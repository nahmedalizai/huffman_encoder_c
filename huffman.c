/*
============================================================================
AUTHOR: NAVEED AHMED ALIZAI
INSTITUTE: TALLINN UNIVESRSITY OF TECHNOLOGY
COURSE: SYSTEM PROGRAMMING
ASSIGNMENT: HUFFMAN COMPRESSION
FALL 2019
============================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHAR 512
#define BIT_MAX_SIZE 56
unsigned int freq_code[CHAR][BIT_MAX_SIZE];
FILE *f_input, *f_output;
typedef unsigned char byte;

struct tree {
    unsigned int ch;
    unsigned int freq;
    int is_leaf;
    struct tree* left;
    struct tree* right;
};
struct tree* forest[CHAR] = {NULL};

struct tree* new_tree_node(unsigned int ch,unsigned int freq, int is_leaf, struct tree* t1, struct tree* t2) {
        struct tree* tree = (struct tree*)malloc(sizeof(struct tree));
        tree->ch = ch;
        tree->freq = freq;
        tree->is_leaf = is_leaf;
        tree->left = t1;
        tree->right = t2;
        return(tree);
}

void sort_forest(int treeCount) {
    struct tree* temp;
    for(int i =0; i<treeCount; i++){
        for(int j = i; j<treeCount; j++){
            if(forest[i]->freq > forest[j]->freq){
                temp = forest[i];
                forest[i] = forest[j];
                forest[j] = temp;
            }
        }
    }
}

void shift_trees(int index, int length){
    for(int i = index; i < length; i++){
        forest[i] = forest[i+1];
    }
}

struct tree* make_tree(int* freq){
    unsigned int treeCount=0;
    for(int i =0; i <CHAR; i++) {
        if(freq[i] > 0){
            forest[treeCount] = new_tree_node(i,freq[i],1, NULL, NULL);
            treeCount++;
        }
    }
    struct tree* t1;
    struct tree* t2;
    if(treeCount == 1){
        t1 = forest[0];
        forest[0] = new_tree_node(0,t1->freq,0,t1,NULL);
    }
    else {
        sort_forest(treeCount);
                // for(int i =0; i <treeCount; i++) {
                //      printf("Tree %d - CH : %c/%d - Freq : %d \n",i,forest[i]->ch,forest[i]->ch, forest[i]->freq);
                // }
        int is_first_time = 1, i = 0;
        while(treeCount > 1){
            t1 = forest[0];
            t2 = forest[1];                                                                                                         
            forest[0] = new_tree_node(0,t2->freq+t1->freq,0,t1,t2);
            shift_trees(1,treeCount);
            treeCount--;
            sort_forest(treeCount);
            is_first_time = 0;
        }
    }
    return forest[0];
}

void generate_codes(struct tree* root, int arr[], int top) {
    if (root->left) {
        arr[top] = 0;
        generate_codes(root->left, arr, top + 1);
    }
    if (root->right) {
        arr[top] = 1;
        generate_codes(root->right, arr, top + 1);
    }
    if (root->is_leaf == 1) {
        //printf("%c: ", root->ch);
        int i;
        for (i = 0; i < top; ++i){
            freq_code[root->ch][i] = arr[i]+1; //1 for 0 and 2 for 1, in order to know where codes end
            //printf("%d",arr[i]);
        }
        //printf("\n");
     }
}

void compress(FILE* fin, FILE* fout, unsigned* padding) {
    unsigned int n, ch;
    byte buf = 0, nbuf = 0;
    byte code[256];
    while ((ch = getc(fin)) != EOF) {
        n = 0;
        for(int i =0; i<BIT_MAX_SIZE; i++){
            if(freq_code[ch][i] != 0){
                if(freq_code[ch][i] ==1)
                    code[n] = 0;
                else if(freq_code[ch][i] ==2)
                    code[n] = 1;
            }
            else
                break;
            n++;
        }
        for (int i = 0; i < n; i++) {
            buf |= code[i] << nbuf;
            nbuf++;
            if (nbuf == 8) {
                fputc(buf, fout);
                nbuf = buf = 0;
            }
        }
    }
    fputc(buf, fout);
    *padding = 8 - nbuf;
}

void decompress(FILE* fin, FILE* fout, struct tree* root, unsigned padding) {
    size_t startpos = ftell(fin); // should be 1028
    fseek(fin, 0L, SEEK_END);
    size_t endpos = ftell(fin); // last byte handling
    fseek(fin, startpos, SEEK_SET);
    int count = endpos - startpos;

    byte buf = 0;
    unsigned int ch;
    int bit,bit_count=8;
    struct tree* node = root;
    while((ch = fgetc(fin)) != EOF) {
        if(count == 0)
            break;
        else if(count == 1)
            bit_count = 8 - padding;
        buf = ch;
        for(int i = 0; i<bit_count; i++){
            bit = buf & 1;
            buf >>= 1;
            if(bit == 0) {
                if(node ->left) {
                    node = node->left;
                }
                if(node->is_leaf == 1) {
                    fputc(node->ch,fout);
                    node = root;
                }
            }
            else if (bit == 1) {
                if(node ->right) {
                    node = node->right;
                }
                if(node->is_leaf == 1) {
                    fputc(node->ch,fout);
                    node = root;
               }
            }
        }
        count--;
    }
}

int main(int argc, char** argv) {
    if (argc != 4) { // Missing argument? return
        fprintf(stderr, "%s", "missing args -c/-d <inputfile> <outputfile>\n");
        return 1;
    }
     if (!strcmp(argv[1], "-c")) { // Compress
        struct tree *binary_tree = (struct tree*)malloc(sizeof(struct tree));
        unsigned int freq[CHAR]={0}, char_count=0;
        unsigned int c;
        f_input = fopen(argv[2],"rb");
        f_output = fopen(argv[3], "wb");
        if (f_input == NULL) {
            fprintf(stderr, "%s", "file not found\n");
            return 1;
        }
        else {
            while((c = getc(f_input)) != EOF) {
                if(freq[c] == 0)
                    char_count++;
                freq[c] ++;
            }
            if(char_count > 0){
                binary_tree = make_tree(freq); //Build Tree
                int arr[CHAR], top = 0;
                generate_codes(binary_tree,arr,top); //Generate Codes
                rewind(f_input);
                size_t padding_pos;
                unsigned padding=0;
                fwrite(&char_count, sizeof(int), 1, f_output);
                for (int i = 0; i < CHAR; i++){
                    if(freq[i] > 0){
                        fwrite(&i, sizeof(char), 1, f_output);
                        fwrite(freq + i, sizeof(int), 1, f_output);
                    }
                }
                padding_pos = ftell(f_output);
                fwrite(&padding, sizeof(char), 1, f_output);
                compress(f_input, f_output, &padding);

                fseek(f_output, padding_pos, SEEK_SET);
                fwrite(&padding, sizeof(char), 1, f_output);
            }
            fclose(f_input);
            fclose(f_output);
        }
    }    
    else if (!strcmp(argv[1], "-d")) { // Decompress
        struct tree *binary_tree_out = (struct tree*)malloc(sizeof(struct tree));
        f_input = fopen(argv[2],"rb");
        f_output = fopen(argv[3], "wb");
        if (f_input == NULL) {
            fprintf(stderr, "%s", "file not found\n");
            return 1;
        }
        else {
            unsigned padding_out;
            unsigned int ch_count_out =0, ch_out = 0, ch_freq_out =0, freq_out[CHAR] ={0};
            fread(&ch_count_out, sizeof(int), 1, f_input);
            for (int i = 0; i < ch_count_out; i++) {
                fread(&ch_out, sizeof(char), 1, f_input);
                fread(&ch_freq_out, sizeof(int), 1, f_input);
                freq_out[ch_out] = ch_freq_out;
            }
            binary_tree_out = make_tree(freq_out);
            fread(&padding_out, sizeof(char), 1, f_input);
            decompress(f_input, f_output, binary_tree_out, padding_out);
        }
        fclose(f_input);
        fclose(f_output);
    }    
    else { // Missing args
        fprintf(stderr, "%s", "Missing arg -c or -d\n"); 
        return 1;
    }
    return 0;
}