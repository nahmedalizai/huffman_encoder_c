#include <stdio.h>

int main() {
  char c = 'a';
  int i;
  for (i = 0; i < 8; i++) {
      printf("%d", !!((c << i) & 0x80));
  }
  printf("\n");

  return 0;
}
