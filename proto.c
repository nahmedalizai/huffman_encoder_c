#include <stdio.h> 
#include <stdlib.h> 

#define CHAR 256  
typedef struct tree {
    /* if left==0, it is a leaf. Otherwise it has two branches */
    struct tree *left;    
    struct tree *right;   /* not  in a leaf */
    int ch;              /* only in a leaf */
    int freq;            /* occurence rate of the tree */
} tree;

tree* create_tree(tree* t1,tree* t2)
{
    tree *new_tree = (tree*)malloc(sizeof(tree));
    if(new_tree == NULL)
    {
        fprintf (stderr, "Out of memory!!! (create_node)\n");
        exit(1);
    }
    new_tree->left = t1;
    new_tree->right = t2;
    new_tree->freq = t1->freq + t2->freq;
    new_tree->ch = 0;
    return new_tree;
}

//ascending
int compare(const void *t1, const void *t2) {
      tree *e1 = (tree *)t1;
      tree *e2 = (tree *)t2;
      return e1->freq - e2->freq;
}

tree* MakeTree(int *freq){
    tree forest[CHAR] = {NULL};
    tree* ptr = NULL;
    int acc = 0;
    ptr = forest;
    int treeCount=0;
    for(int i =0; i <CHAR; i++) {
        if(freq[i] > 0){
            ptr->left = NULL;
            ptr->right = NULL;
            ptr->ch = i;
            ptr-> freq = freq[i];
            treeCount++;
            ptr++;
        }
    }

    qsort(forest, CHAR, sizeof(struct tree), compare);

    tree* giant_tree = NULL;
    tree* cursor = NULL;
    int remaining = CHAR;
    int index = 0;
    while(remaining > 1) {
        if(forest[index].freq > 0) {
            printf("\nIndex : %d From forest: Character: %C ASCII Code %d,  Frequency: %d\n",index, forest[index].ch,forest[index].ch, forest[index].freq);
            //TODO Character (CHAR - 2 ) to allow compression if only one tree exists
            if(giant_tree == NULL && index < CHAR - 2) { // Runs only first time when giant_tree is null
                giant_tree = create_tree(&forest[index],&forest[index+1]);
                index++;
            }
            else {
                giant_tree = create_tree(giant_tree,&forest[index]);
            }
//            giant_tree = create_tree(forest[index],forest[index+1]);
        
        }
        index++;
        remaining--;
    }
    return giant_tree;
}

int main () {
    tree* binary_tree = NULL;
    int freq[CHAR]={0};
    char c;
    FILE *fptr = fopen("file.txt","r");
    if (fptr == NULL) {
        return 0;
    }
    else {
       while(!feof(fptr)) { 
           c = getc(fptr);
           freq[c] ++;
        }
        binary_tree = MakeTree(freq);
        // for(int i=0; i< CHAR; i++)
        //  printf("%d : %c - %d \n",i, i,freq[i]);
    }
    return 0;
}
